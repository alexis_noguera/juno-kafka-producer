package com.junoKafka.checkoutProducer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CheckoutProducerApplication

fun main(args: Array<String>) {
	runApplication<CheckoutProducerApplication>(*args)
}
