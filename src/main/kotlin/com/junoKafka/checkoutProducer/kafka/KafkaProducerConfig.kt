package com.junoKafka.checkoutProducer.kafka

import com.fasterxml.jackson.databind.JsonNode
import com.junoKafka.checkoutProducer.model.CheckoutOrder
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.kafka.support.serializer.JsonSerializer

@Configuration
class KafkaProducerConfig {
    @Bean
    fun producerFactory(): ProducerFactory<String, CheckoutOrder> {
        val configProps = HashMap<String, Any?>()
        configProps[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
        configProps[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        configProps[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = JsonSerializer::class.java
        return DefaultKafkaProducerFactory(configProps);
    }

    @Bean
    fun kafkaTemplateAny(): KafkaTemplate<String, CheckoutOrder> {
        return KafkaTemplate(producerFactory())
    }
    @Bean
    fun producerFactoryJson(): ProducerFactory<String, JsonNode> {
        val configProps = HashMap<String, Any?>()
        configProps[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
        configProps[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        configProps[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = JsonSerializer::class.java
        return DefaultKafkaProducerFactory(configProps);
    }

    @Bean
    fun kafkaTemplateString(): KafkaTemplate<String, JsonNode> {
        return KafkaTemplate(producerFactoryJson())
    }
}