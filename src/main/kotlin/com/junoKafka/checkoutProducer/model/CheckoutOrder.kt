package com.junoKafka.checkoutProducer.model

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate

data class CheckoutOrder(
    val orderID: Long,
    val orderType: String,
    val paymentDetail: PaymentDetail,
    val shippingGroups: List<ShippingGroup>,
    var submittedDate: LocalDate,
    val version: String
)

data class PaymentDetail(
    val salesChannel: String
)

data class ShippingGroup(
    val deliveryDetails: DeliveryDetails,
    val items: List<Item>,
    val pickingDetails: PickingDetails,
    val shippingGroupID: String
)

data class DeliveryDetails(
    val comment: String,
    val customer: Customer,
    val deliveryChannel: String,
    val deliveryDate: LocalDate,
    val deliverySlot: String
)

data class Item(
    val barcode: String,
    val description: String,
    val inventoryType: String,
    val itemNumber: String,
    val prices: Prices,
    val productNumber: Int,
    val quantity: Int,
    val vendorID: Int
)

data class PickingDetails(
    val pickingDate: LocalDate,
    val pickingStore: String
)

data class Customer(
    @JsonProperty(value = "RUT")
    val rut: String,
    val address: Address,
    val email: String,
    val firstName: String,
    val lastName: String,
    val phone: String
)

data class Address(
    val city: String,
    val commune: String,
    val houseNumber: String,
    val region: String,
    val streetName: String,
    val streetNumber: String
)

data class Prices(
    val grossUnitPrice: Int
)
