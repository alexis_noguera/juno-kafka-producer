package com.junoKafka.checkoutProducer.controller

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/publish")
class CheckoutProducerController(private val kafkaTemplate: KafkaTemplate<String, JsonNode>) {
    @PostMapping
    fun publishKafkaCheckoutOrder(@RequestBody jsonNode: JsonNode): JsonNode {
        this.kafkaTemplate.send("picking-order-finalized-gm-v1", jsonNode)
        return jsonNode
    }

}